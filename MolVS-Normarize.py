from rdkit import Chem
from molvs.normalize import Normalizer, Normalization

old_smiles = "[Na]OC(=O)c1ccc(C[S+2]([O-])([O-]))cc1"
print("PREV:" + old_smiles)
old_mol = Chem.MolFromSmiles(old_smiles)
normalizer = Normalizer(normalizations=[Normalization('Sulfone to S(=O)(=O)', '[S+2:1]([O-:2])([O-:3])>>[S+0:1](=[O-0:2])(=[O-0:3])')])
new_mol = normalizer.normalize(old_mol)
new_smiles = Chem.MolToSmiles(new_mol)
print("NEW:" + new_smiles)