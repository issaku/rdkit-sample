from rdkit import Chem
from rdkit.Chem import rdFMCS
from rdkit.Chem.Draw import IPythonConsole
from rdkit import RDConfig
from rdkit.Chem import FragmentCatalog
 
mol1 = Chem.MolFromSmiles("Cc1ccccc1")
mol2 = Chem.MolFromSmiles( "CCc1ccccc1" )
mol3 = Chem.MolFromSmiles( "Oc1ccccc1" )
mol4 = Chem.MolFromSmiles( "COc1ccccc1" )

res = rdFMCS.FindMCS([mol1,mol2,mol3,mol4], threshold=0.5)
res2 = rdFMCS.FindMCS([mol4,mol3,mol2,mol1], threshold=0.5)

print(res.smartsString)
print(res2.smartsString)