from molvs.charge import Reionizer, Uncharger

uncharger = Uncharger()
mol = Chem.MolFromSmiles("c1cccc[nH+]1")
print("prev:" + Chem.MolToSmiles(mol))
mol2 = uncharger(mol)
print("after:" + Chem.MolToSmiles(mol2))